/*
 *
 * (C) COPYRIGHT 2014-2015 ARM Limited. All rights reserved.
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file mali_kbase_hwaccess_pm.h
 * HW access power manager common APIs
 */

#ifndef _KBASE_HWACCESS_PM_H_
#define _KBASE_HWACCESS_PM_H_

#include <mali_midg_regmap.h>
#include <linux/atomic.h>

#include <mali_kbase_pm_defs.h>

/* Forward definition - see mali_kbase.h */
struct kbase_device;

/**
 * Initialize the power management framework.
 *
 * Must be called before any other power management function
 *
 * @param kbdev The kbase device structure for the device (must be a valid
 *              pointer)
 *
 * @return 0 if the power management framework was successfully
 *         initialized.
 */
int kbase_hwaccess_pm_init(struct kbase_device *kbdev);

/**
 * Terminate the power management framework.
 *
 * No power management functions may be called after this (except
 * @ref kbase_pm_init)
 *
 * @param kbdev The kbase device structure for the device (must be a valid
 *              pointer)
 */
void kbase_hwaccess_pm_term(struct kbase_device *kbdev);

/**
 * kbase_hwaccess_pm_powerup - Power up the GPU.
 * @kbdev: The kbase device structure for the device (must be a valid pointer)
 * @flags: Flags to pass on to kbase_pm_init_hw
 *
 * Power up GPU after all modules have been initialized and interrupt handlers
 * installed.
 *
 * Return: 0 if powerup was successful.
 */
int kbase_hwaccess_pm_powerup(struct kbase_device *kbdev,
		unsigned int flags);

/**
 * Halt the power management framework.
 *
 * Should ensure that no new interrupts are generated, but allow any currently
 * running interrupt handlers to complete successfully. The GPU is forced off by
 * the time this function returns, regardless of whether or not the active power
 * policy asks for the GPU to be powered off.
 *
 * @param kbdev The kbase device structure for the device (must be a valid
 *              pointer)
 */
void kbase_hwaccess_pm_halt(struct kbase_device *kbdev);

/**
 * Perform any backend-specific actions to suspend the GPU
 *
 * @param kbdev The kbase device structure for the device (must be a valid
 *              pointer)
 */
void kbase_hwaccess_pm_suspend(struct kbase_device *kbdev);

/**
 * Perform any backend-specific actions to resume the GPU from a suspend
 *
 * @param kbdev The kbase device structure for the device (must be a valid
 *              pointer)
 */
void kbase_hwaccess_pm_resume(struct kbase_device *kbdev);

/**
 * Perform any required actions for activating the GPU. Called when the first
 * context goes active.
 *
 * @param kbdev The kbase device structure for the device (must be a valid
 *              pointer)
 */
void kbase_hwaccess_pm_gpu_active(struct kbase_device *kbdev);

/**
 * Perform any required actions for idling the GPU. Called when the last
 * context goes idle.
 *
 * @param kbdev The kbase device structure for the device (must be a valid
 *              pointer)
 */
void kbase_hwaccess_pm_gpu_idle(struct kbase_device *kbdev);

#endif // ifndef _KBASE_HWACCESS_PM_H_
