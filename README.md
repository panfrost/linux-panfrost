Oolong
==========

Kernel module for testing forked from the Mali kernel module for the
[chai](https://notabug.org/cafe/chai) project. Mainline kernel support uses
patches by Pierre-Hugues Husson from
[rockchip-forwardports](https://github.com/phhusson/rockchip_forwardports).

Usage (Rockchip)
-----------------

Clone oolong and Linux into the same folder.

Compile module:

    cd oolong && ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make M=$PWD -C ../linux CONFIG_MALI_MIDGARD=m CONFIG_MALI_DMA_FENCE=y CONFIG_MALI_EXPERT=y CONFIG_MALI_PLATFORM_THIRDPARTY=y CONFIG_MALI_PLATFORM_THIRDPARTY_NAME=rk && cd ..

This command generates mali_kbase.ko. Load it:

    insmod oolong/mali_kbase.ko
