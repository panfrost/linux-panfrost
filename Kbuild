#
# (C) COPYRIGHT 2012,2014 ARM Limited. All rights reserved.
#
# This program is free software and is provided to you under the terms of the
# GNU General Public License version 2 as published by the Free Software
# Foundation, and any use by you of this program is subject to the terms
# of such GNU licence.
#
# A copy of the licence is included with the program, and can also be obtained
# from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA  02110-1301, USA.
#
#

# Paths required for build
KBASE_PATH = $(src)
UMP_PATH = $(src)/../../../base

# Set up our defines, which will be passed to gcc
DEFINES = -DMALI_RELEASE_NAME=\"$(MALI_RELEASE_NAME)\"

DEFINES += -I$(srctree)/drivers/staging/android

# Use our defines when compiling
ccflags-y += $(DEFINES) -I$(KBASE_PATH)   -I$(UMP_PATH) -I$(srctree)/include/linux
subdir-ccflags-y += $(DEFINES) -I$(KBASE_PATH)   -I$(OSK_PATH) -I$(UMP_PATH) -I$(srctree)/include/linux

SRC := \
	mali_kbase_device.c \
	mali_kbase_cache_policy.c \
	mali_kbase_mem.c \
	mali_kbase_mmu.c \
	mali_kbase_jd.c \
	mali_kbase_jm.c \
	mali_kbase_gpuprops.c \
	mali_kbase_js.c \
	mali_kbase_js_ctx_attr.c \
	mali_kbase_event.c \
	mali_kbase_context.c \
	mali_kbase_pm.c \
	mali_kbase_hw.c \
	mali_kbase_mem_linux.c \
	mali_kbase_core_linux.c \
	mali_kbase_replay.c \
	mali_kbase_mmu_mode_lpae.c \
	mali_kbase_disjoint_events.c \
	mali_kbase_smc.c \
	mali_kbase_mem_pool.c \
	mali_kbase_device_hw.c \
	mali_kbase_irq_linux.c \
	mali_kbase_jm_as.c \
	mali_kbase_jm_hw.c \
	mali_kbase_jm_rb.c \
	mali_kbase_js_affinity.c \
	mali_kbase_js_backend.c \
	mali_kbase_mmu_hw_direct.c \
	mali_kbase_pm_backend.c \
	mali_kbase_pm_driver.c \
	mali_kbase_pm_metrics.c \
	mali_kbase_pm_policy.c

# Job Scheduler Policy: Completely Fair Scheduler
SRC += mali_kbase_js_policy_cfs.c

ccflags-y += -I$(KBASE_PATH)

# Tell the Linux build system from which .o file to create the kernel module
obj-$(CONFIG_MALI_MIDGARD) += mali_kbase.o

# Tell the Linux build system to enable building of our .c files
mali_kbase-y := $(SRC:.c=.o)
