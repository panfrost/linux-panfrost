/*
 *
 * (C) COPYRIGHT 2014-2016 ARM Limited. All rights reserved.
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include <mali_kbase.h>
#include <mali_kbase_device_internal.h>
#include <mali_kbase_irq_internal.h>

#include <linux/interrupt.h>

/* GPU IRQ Tags */
#define	JOB_IRQ_TAG	0
#define MMU_IRQ_TAG	1
#define GPU_IRQ_TAG	2

#define IRQ_HANDLER(name, done, reg, fault) \
	static irqreturn_t kbase_##name##_irq_handler(int irq, void *data) \
	{ \
		unsigned long flags; \
		struct kbase_device *kbdev = data; \
		u32 val; \
 \
		spin_lock_irqsave(&kbdev->pm.backend.gpu_powered_lock, flags); \
 \
		if (!kbdev->pm.backend.gpu_powered) { \
			/* GPU is turned off - IRQ is not for us */ \
			spin_unlock_irqrestore(&kbdev->pm.backend.gpu_powered_lock, \
										flags); \
			return IRQ_NONE; \
		} \
 \
		if (fault) atomic_inc(&kbdev->faults_pending); \
		val = kbase_reg_read(kbdev, reg, NULL); \
 \
		spin_unlock_irqrestore(&kbdev->pm.backend.gpu_powered_lock, flags); \
 \
		if (!val) { \
			if (fault) atomic_dec(&kbdev->faults_pending); \
			return IRQ_NONE; \
		} \
 \
		done(kbdev, val); \
 \
		return IRQ_HANDLED; \
	} \

IRQ_HANDLER(job, kbase_job_done, JOB_CONTROL_REG(JOB_IRQ_STATUS), false);
IRQ_HANDLER(mmu, kbase_mmu_interrupt, MMU_REG(MMU_IRQ_STATUS), true);
IRQ_HANDLER(gpu, kbase_gpu_interrupt, GPU_CONTROL_REG(GPU_IRQ_STATUS), false);

static irq_handler_t kbase_handler_table[] = {
	[JOB_IRQ_TAG] = kbase_job_irq_handler,
	[MMU_IRQ_TAG] = kbase_mmu_irq_handler,
	[GPU_IRQ_TAG] = kbase_gpu_irq_handler,
};

int kbase_install_interrupts(struct kbase_device *kbdev)
{
	u32 nr = ARRAY_SIZE(kbase_handler_table);
	int err;
	u32 i;

	for (i = 0; i < nr; i++) {
		err = request_irq(kbdev->irqs[i].irq, kbase_handler_table[i],
				kbdev->irqs[i].flags | IRQF_SHARED,
				dev_name(kbdev->dev),
				kbdev);
		if (err) {
			dev_err(kbdev->dev, "Can't request interrupt %d (index %d)\n",
							kbdev->irqs[i].irq, i);
			goto release;
		}
	}

	return 0;

 release:
	while (i-- > 0)
		free_irq(kbdev->irqs[i].irq, kbdev);

	return err;
}

void kbase_release_interrupts(struct kbase_device *kbdev)
{
	u32 nr = ARRAY_SIZE(kbase_handler_table);
	u32 i;

	for (i = 0; i < nr; i++) {
		if (kbdev->irqs[i].irq)
			free_irq(kbdev->irqs[i].irq, kbdev);
	}
}

void kbase_synchronize_irqs(struct kbase_device *kbdev)
{
	u32 nr = ARRAY_SIZE(kbase_handler_table);
	u32 i;

	for (i = 0; i < nr; i++) {
		if (kbdev->irqs[i].irq)
			synchronize_irq(kbdev->irqs[i].irq);
	}
}
