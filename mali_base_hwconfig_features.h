/*
 *
 * (C) COPYRIGHT 2015-2016 ARM Limited. All rights reserved.
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#ifndef _BASE_HWCONFIG_FEATURES_H_
#define _BASE_HWCONFIG_FEATURES_H_

enum base_hw_feature {
	BASE_HW_FEATURE_JOBCHAIN_DISAMBIGUATION,
	BASE_HW_FEATURE_PWRON_DURING_PWROFF_TRANS,
	BASE_HW_FEATURE_33BIT_VA,
	BASE_HW_FEATURE_FLUSH_REDUCTION,
	BASE_HW_FEATURE_COHERENCY_REG,
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_generic[] = {
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_t60x[] = {
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_t62x[] = {
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_t72x[] = {
	BASE_HW_FEATURE_33BIT_VA,
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_t76x[] = {
	BASE_HW_FEATURE_JOBCHAIN_DISAMBIGUATION,
	BASE_HW_FEATURE_PWRON_DURING_PWROFF_TRANS,
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_tFxx[] = {
	BASE_HW_FEATURE_JOBCHAIN_DISAMBIGUATION,
	BASE_HW_FEATURE_PWRON_DURING_PWROFF_TRANS,
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_t83x[] = {
	BASE_HW_FEATURE_33BIT_VA,
	BASE_HW_FEATURE_JOBCHAIN_DISAMBIGUATION,
	BASE_HW_FEATURE_PWRON_DURING_PWROFF_TRANS,
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_t82x[] = {
	BASE_HW_FEATURE_33BIT_VA,
	BASE_HW_FEATURE_JOBCHAIN_DISAMBIGUATION,
	BASE_HW_FEATURE_PWRON_DURING_PWROFF_TRANS,
	BASE_HW_FEATURE_END
};

static const enum base_hw_feature base_hw_features_tMIx[] = {
	BASE_HW_FEATURE_JOBCHAIN_DISAMBIGUATION,
	BASE_HW_FEATURE_PWRON_DURING_PWROFF_TRANS,
	BASE_HW_FEATURE_FLUSH_REDUCTION,
	BASE_HW_FEATURE_COHERENCY_REG,
	BASE_HW_FEATURE_END
};

#endif // ifndef _BASE_HWCONFIG_FEATURES_H_
