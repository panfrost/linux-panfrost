/*
 *
 * (C) COPYRIGHT 2015-2016 ARM Limited. All rights reserved.
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#ifndef _BASE_HWCONFIG_ISSUES_H_
#define _BASE_HWCONFIG_ISSUES_H_

enum base_hw_issue {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_6367,
	BASE_HW_ISSUE_6787,
	BASE_HW_ISSUE_8408,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_9510,
	BASE_HW_ISSUE_10327,
	BASE_HW_ISSUE_10676,
	BASE_HW_ISSUE_10797,
	BASE_HW_ISSUE_10817,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_11035,
	BASE_HW_ISSUE_T76X_26,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3542,
	BASE_HW_ISSUE_T76X_3556,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_TMIX_7940,
	BASE_HW_ISSUE_TMIX_8042,
	BASE_HW_ISSUE_TMIX_8133,
	BASE_HW_ISSUE_TMIX_8138,
	BASE_HW_ISSUE_TMIX_8206,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_generic[] = {
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t60x_r0p1[] = {
	BASE_HW_ISSUE_6367,
	BASE_HW_ISSUE_6787,
	BASE_HW_ISSUE_8408,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_9510,
	BASE_HW_ISSUE_10676,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11035,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t62x_r0p1[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10327,
	BASE_HW_ISSUE_10676,
	BASE_HW_ISSUE_10817,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_11035,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t62x_r1p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10817,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t62x_r1p1[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10817,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t76x_r0p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_T76X_26,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3542,
	BASE_HW_ISSUE_T76X_3556,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t76x_r0p1[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_T76X_26,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3542,
	BASE_HW_ISSUE_T76X_3556,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t76x_r0p1_50rel0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_26,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3542,
	BASE_HW_ISSUE_T76X_3556,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t76x_r0p2[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_T76X_26,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3542,
	BASE_HW_ISSUE_T76X_3556,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t76x_r0p3[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_26,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3542,
	BASE_HW_ISSUE_T76X_3556,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t76x_r1p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t72x_r0p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10797,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t72x_r1p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10797,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t72x_r1p1[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10797,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_t72x[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10797,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_t76x[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_t60x[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_t62x[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_11020,
	BASE_HW_ISSUE_11024,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3964,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_tFRx_r0p1[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_tFRx_r0p2[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_tFRx_r1p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_tFRx_r2p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_tFRx[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t86x_r0p2[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t86x_r1p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t86x_r2p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3966,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_t86x[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t83x_r0p1[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t83x_r1p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_t83x[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t82x_r0p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3964,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t82x_r0p1[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_t82x_r1p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_10883,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_T76X_3960,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_t82x[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_T76X_1909,
	BASE_HW_ISSUE_T76X_1963,
	BASE_HW_ISSUE_T76X_3086,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3793,
	BASE_HW_ISSUE_T76X_3979,
	BASE_HW_ISSUE_TMIX_7891,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_tMIx_r0p0_05dev0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_T76X_3953,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_TMIX_8042,
	BASE_HW_ISSUE_TMIX_8133,
	BASE_HW_ISSUE_TMIX_8138,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_tMIx_r0p0[] = {
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_TMIX_7940,
	BASE_HW_ISSUE_TMIX_8042,
	BASE_HW_ISSUE_TMIX_8133,
	BASE_HW_ISSUE_TMIX_8138,
	BASE_HW_ISSUE_TMIX_8206,
	BASE_HW_ISSUE_END
};

static const enum base_hw_issue base_hw_issues_model_tMIx[] = {
	BASE_HW_ISSUE_5736,
	BASE_HW_ISSUE_9435,
	BASE_HW_ISSUE_T76X_3700,
	BASE_HW_ISSUE_TMIX_7891,
	BASE_HW_ISSUE_TMIX_7940,
	BASE_HW_ISSUE_TMIX_8042,
	BASE_HW_ISSUE_TMIX_8133,
	BASE_HW_ISSUE_TMIX_8138,
	BASE_HW_ISSUE_TMIX_8206,
	GPUCORE_1619,
	BASE_HW_ISSUE_END
};

#endif // ifndef _BASE_HWCONFIG_ISSUES_H_
